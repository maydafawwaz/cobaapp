class ArticlesController < ApplicationController
    before_action :set_article, only: [:show, :update, :destroy]
    #GET /articles
    def index
        @article = Article.all
        render json: @article.order("id DESC")
    end
    # POST /articles/
    def create
        @article = Article.new(article_params)
        if @article.save
            render json: @article, status: :created
        else
            render json: @article.errors
        end
     
    end

    # GET /articles/:id
    def show
        render json: @article
    end

    # PUT /articles/:id
    def update
        @article = Article.find(params[:id])
        render json: @article
        if @article.update(article_params)
            render json: @article
        else
            render 'edit'
        end
            
    end
    
    #DELETE /articles/:id
    def destroy
        @article.destroy
    end

    private
    def article_params
      params.require(:article).permit(:title, :content, :author)
    end

    def set_article
        @article = Article.find(params[:id])
    end
end
